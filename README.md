# hello-world-docker-compose
Hello world python app using docker compose

## How to create, edit, deploy a container image

### Prerequisites: 
	DockerHub account
	Docker desktop installed on local machine
	- installation includes CLI tools, BuildKit, Docker Engine and Docker Compose
	Git - Github or other git tools 

Docker compose provides a simpiflied way to run multiple services in a single container using docker images. In this example, we have a web service and a redis service

1. Clone docker-demo repository and open on your local machine

2. Review files
    - dockerfile: Our docker image build script which tells docker what to package within the image
    - app.py: The python executable file for running our app
    - requirements.txt: Lists all the libraries to install for the python app
    - Pipfile: ensures the correct version of dependencies in requirements.txt, and their required dependencies are installed
    - docker-compose.yaml: Used for running multiple containers

3. Build and run the app
    - Run `docker-compose up` to build and start the app
        - The command builds the needed images to run the complete application in a container, including:
            - Networking comopnents
            - Builds iages for web and redis
            - Attaches the components
            - Exposes necessary ports
            - Serves the web page

4. Access the app
    - Open localhost:8000 from your web browser
    - Run `curl localhost:8000`
    - To stop the app, run `docker-compose down` from a seperate terminal window or Press CTRL+C to quit from the same teerminal window

5. Inspect images
    - Run `docker image ls` to list the available images
    ```
    REPOSITORY                       TAG       IMAGE ID       CREATED             SIZE
    hello-world-docker-compose_web   latest    ec1550d18f4c   8 minutes ago       189MB
    hello-world-docker               new       0a0b38a0fab5   About an hour ago   125MB
    hello-world-docker               latest    eff3d90d89f0   2 hours ago         125MB
    hello-world-docker               base      4b2b947d560b   3 hours ago         125MB
    test                             latest    f872ce0184a4   2 days ago          927MB
    docker/getting-started           latest    bd9a9f733898   6 days ago          28.8MB
    redis                            alpine    3900abf41552   2 months ago        32.4MB
    ```
    - As you can see, we have two new images, `hello-world-docker-compose_web` and `redis` which were created with the `docker-compose up` command. 
    - Run `docker inspect [tag/id]` to inspect an image

6. Docker compose commands
    ```
    Define and run multi-container applications with Docker.

    Usage:
    docker-compose [-f <arg>...] [--profile <name>...] [options] [COMMAND] [ARGS...]
    docker-compose -h|--help

    Options:
    -f, --file FILE             Specify an alternate compose file
                                (default: docker-compose.yml)
    -p, --project-name NAME     Specify an alternate project name
                                (default: directory name)
    --profile NAME              Specify a profile to enable
    --verbose                   Show more output
    --log-level LEVEL           Set log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
    --no-ansi                   Do not print ANSI control characters
    -v, --version               Print version and exit
    -H, --host HOST             Daemon socket to connect to

    --tls                       Use TLS; implied by --tlsverify
    --tlscacert CA_PATH         Trust certs signed only by this CA
    --tlscert CLIENT_CERT_PATH  Path to TLS certificate file
    --tlskey TLS_KEY_PATH       Path to TLS key file
    --tlsverify                 Use TLS and verify the remote
    --skip-hostname-check       Don't check the daemon's hostname against the
                                name specified in the client certificate
    --project-directory PATH    Specify an alternate working directory
                                (default: the path of the Compose file)
    --compatibility            If set, Compose will attempt to convert deploy
                                keys in v3 files to their non-Swarm equivalent
    Commands:
    build              Build or rebuild services
    bundle             Generate a Docker bundle from the Compose file
    config             Validate and view the Compose file
    create             Create services
    down               Stop and remove containers, networks, images, and volumes
    events             Receive real time events from containers
    exec               Execute a command in a running container
    help               Get help on a command
    images             List images
    kill               Kill containers
    logs               View output from containers
    pause              Pause services
    port               Print the public port for a port binding
    ps                 List containers
    pull               Pull service images
    push               Push service images
    restart            Restart services
    rm                 Remove stopped containers
    run                Run a one-off command
    scale              Set number of containers for a service
    start              Start services
    stop               Stop services
    top                Display the running processes
    unpause            Unpause services
    up                 Create and start containers
    version            Show the Docker-Compose version information
    ```